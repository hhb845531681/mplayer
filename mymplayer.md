# 一、基本功能
## 1.歌单操作
### ①添加歌曲
### ②删除歌曲
### ③歌曲目录陈列
### ④添加一个文件夹下所有的.mp3格式文件
## 2.歌曲控制
### ①点歌
### ②暂停/播放
### ③上一首
### ④下一首
### ⑤快进
### ⑥快退
## 3.音量控制
### ①静音/取消静音
### ②手动调节音量
## 4.其他
### ①歌曲播放结束，自动播放下一首
### ②同步显示歌词

# 二、流程图
![](https://i.imgur.com/Eu6XGeq.png)

# 三、实现与mplayer的通信
## 1.mplayer简介
>MPlayer是一款开源的多媒体播放器，以GNU通用公共许可证发布。此款软件可在各主流作业系统使用，例如Linux和其他类Unix作业系统、微软的视窗系统及苹果电脑的Mac OS X系统。MPlayer是建基于命令行界面，在各作业系统可选择安装不同的图形界面。
>
>默认mplayer是从键盘上获得控制信息，但它提供一种更为灵活的控制方式，用来进行播放控制——slave模式，在这个模式下，它会从标准输入读一个换行符（\n）隔开的命令（这也是为什么后面去获得歌曲信息时要往命令中加入\n的原因，但是切歌这些操作貌似不用？），所以这次主要用的是它的slave工作方式。

## 2.exec函数启动mplayer的命令
>execl("./mplayer_arm"," mplayer_arm ", "-ac", "mad","-slave", "-quiet","-idle","-input", "file=./fifo_cmd","./media/sad_or_happy.mp3", NULL);

参数：1："-ac", "mad" -指定使用mad解码器。

2："-slave" -MPlayer 从标准输入读取以新行 (\n) 分隔开的命令行。

3："-quiet" -使得控制台输出消息较少。

4："-idle" -播放文件至文件末尾后mplayer不退出

5："-intput", "file=./tmp/cmd_fifo"  -Mplayer 通过命名管道”./tmp/cmd_fifo”获取命令。

6：song_path_name  -歌曲的路径+文件名首地址。

7：NULL  -Mplayer可以通过它来判断到底有多少个参数，这个必不可少。

## 3.mplayer常用命令
>loadfile string  //string为歌曲路径+文件名(例：“loadfile ../easylove.mp3”)。

>volume x 1 //设置音量，x为音量的大小，0<=x<=100（例：volume 50 1）

>mute 1/0 //静音开关。1：静音；0：取消静音。

>pause //暂停/取消暂停。

>seek value //快进或快递参数value指定的秒数。当value为正时，快进；当value为负时，快退。

>get_percent_pos //获得文件的播放进度（百分比：0--100）。

>get_time_pos //获得文件的当前位置，以秒为单位，精确到小数位1位。

>get_file_name //获得文件的文件名。

>get_time_length //获得文件的长度，以秒为单位。

>get_meta_album //获得文件的 '专辑' 的元数据。

>get_meta_artist //获得文件的 '艺术家' 的元数据。

>get_meta_comment //获得文件的 '评论' 的元数据。

>get_meta_genre //获得文件的 '流派' 的元数据。

>get_meta_title //获得文件的 '标题' 的元数据。

>get_meta_year //获得文件的 '年份' 的元数据。

## 4.创建无名管道和有名管道
### 利用pipe函数初始化无名管道，并在子进程中关闭读端fd[0]，在父进程中关闭写端fd[1]
### 利用mkfifo函数创建有名管道文件，后续创建线程对管道进行读写命令

# 四、实现步骤
## 1.main函数
### ①首先进行管道的创建
### ②定义一个信号量，控制线程2与线程3的执行顺序（每次线程2发送信息后，线程3再去无名管道读取信息）
### ③fork子进程，调用exec函数启动mplayer
### ④创建线程1：向有名管道发送命令实现播放、暂停、上/下一首等歌曲控制的功能
### 线程2：向有名管道发送命令用来获取歌曲的播放进度
### 线程3：从无名管道中读取mplayer反馈的进度，实现歌词同步显示，且自动播放下一首
### ⑤调用atexit函数退出mplayer（在父进程开头调用，不能在最后调用！）

## 2.线程1
>pthread_create(&tid1,NULL,getcmd_thread,NULL);

### 1）歌单操作
#### ①添加歌曲
创建歌曲链表，放入一个文件中 song.txt
#### ②添加文件夹下歌曲
利用opendir（文件夹路径）函数打开文件夹，函数返回一个目录指针。再用readdir(目录指针)函数读取目录中的信息，此函数每次读一个文件，直到返回NULL表示读到目录尾为止，每次会返回一个结构体，结构体内含读到文件的信息：文件名、文件大小、文件类型等，通过比较文件名来判断是否为一个mp3文件，组成路径字符串放入文件中保存（注意保存时的歌曲序号！）。
![](https://i.imgur.com/ApyzJJ3.png)
#### ③删除歌曲
读取文件，删除链表中的节点
#### ④陈列歌曲
读取文件，遍历链表

### 2）播放控制
**因为mplayer有一个特点，暂停之后不论有什么操作，他都会取消暂停，所以每个操作后都要将暂停标志复原**
#### ①点歌
首先界面陈列出歌曲，用户输入相应的歌曲序号，通过遍历链表比较序号，再将相应序号的歌曲的路径传入有名管道（记得前面加"loadfile"）。
#### ②暂停/播放
将"p\n"传入有名管道（注意一定要加\n ！！！）。
#### ③上一首
与点歌类似的思路，遍历链表找到为当前歌曲序号减1的歌序号，传入路径。
#### ④下一首
遍历链表，将下一节点的歌曲路径传入。
#### ⑤快进
#### ⑥快退
注意格式：seek x\n ，x为正——快进，为负——快退

### 3）音量控制
#### ①静音、取消静音
格式为：mute 0/1\n ，0为静音，1为取消静音
#### ②调节音量
格式：volume x 1\n ，0<=x<=100，1不可少，注意\n

## 3.线程2
>pthread_create(&tid2,NULL,sndfd_thread,NULL);

每隔一秒向有名管道发送获取歌曲当前进度的信息:
>"get_time_pos\nget_percent_pos\n"

同时每次发送消息前查看是否暂停，有暂停不能发送消息，否则会打断暂停状态。
![](https://i.imgur.com/MmgUgyJ.png)

## 4.线程3
>pthread_create(&tid3,NULL,readfd_thread,NULL);

每隔一秒读取无名管道的信息获取播放进度，存入buf字符数组中。
但是因为mplayer返回的信息会带有ANS_TIME_POSITION等的前缀，所以通过函数deal_data（）处理信息，拿到有效的数据。

### 1）deal_data（）函数
定义字符指针p，ps
>char *p = NULL;

>char *ps = NULL;

通过strtok函数拿到字符串中第一个以\n结尾的字符串地址
>p = strtok(msg_buf,"\n");

拿到下一个以\n结尾的字符串地址
>p = strtok(NULL,"\n");

通过strstr函数得到相同字符出现的首地址
>strstr(p,"ANS_TIME_POSITION");//得到歌曲当前播放的时间

通过比较当前播放时间显示相应的歌词 

>strstr(p,"ANS_PERCENT_POSITION");//得到歌曲当前播放的进度

通过得到播放进度，与99%比较，达到自动播放下一首的效果

![](https://i.imgur.com/4AW9iPc.png)

### 2）形成歌词链表函数struct lrc *GetLrc(char *lrcpath)
歌词结构体：

    struct lrc
    {
    	int time;
    	char lrctext[200];
    	struct lrc *next;
    };

①利用fseek(),ftell()函数得到lrc文件的大小，将整个文件的内容拷贝到一个字符数组中，形成一个字符串。

    fd = fopen(lrcpath,"r");
    fseek(fd,0,2); //文件描述符偏移到文件尾
    size = ftell(fd); //计算文件大小（当前文件描述符到文件头的字节数） 
    rewind(fd);
    char *buf = (char *)malloc(size + 1);
    rsize = fread(buf,1,size,fd); //注意fread返回值为真是读取的项数，所以我们一个一个的读，读size次，读到buf里
    fclose(fd);	
	*(buf + rsize) = '\0';  //将最后一位置成'\0'

②通过换行符"\n"得到每一行的歌词，因为会出现多个时间点同一句歌词的现象，因此统计出现时间点的个数count

    while( *pb != '\n')
    {
    	temp[i] = *pb;		
		pb++;		
    	i++;		
    }		
    temp[i] = '\0';   //temp储存一行的歌词信息		
    pb++;		
    for(j=0;j<i;j++)		
    {			
    	if(temp[j] == '.')  //统计此行歌词出现的次数			
   		{		
    		count++;			
    	}
	}

③将歌词的时间点转换成秒（因为前面mplayer返回的歌曲当前时间是以秒为单位），
存入歌词结构体，形成链表
![](https://i.imgur.com/VwfZKWv.png)

# 五、终端效果
![](https://i.imgur.com/vOwFxQx.png)