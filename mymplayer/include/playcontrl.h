#ifndef _PLAYCONTRL_H
#define _PLAYCONTRL_H

int contrlmenu();

int volumemenu();

void playcontrl();

void volumecontrl();

int playsong();

int pausesong();

int nextsong();

int lastsong();

int fastsong();

int slowsong();

void exitsong();

int novolume();

int havevolume();

int changevolume();

void mp3_lrc();

void deal_data(char *buf);

void *getcmd_thread(void *arg);

void *sndfd_thread(void *arg);

void *readfd_thread(void *arg);

#endif