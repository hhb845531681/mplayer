#ifndef _SONGLRC_H
#define _SONGLRC_H

struct lrc
{
	int time;
	char lrctext[200];
	struct lrc *next;
};

struct lrc *lrclist(struct lrc *head,struct lrc *pnew);

struct lrc *GetLrc(char *lrcpath);

#endif
