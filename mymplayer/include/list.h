#ifndef _LIST_H
#define _LIST_H


struct songnode
{
	int songid;
	char path[100];
	struct songnode *next;
};

typedef struct songnode Song;


Song *read_song();

int add_song();

int add_folder();       //添加一个文件夹，将此文件夹下的.mp3文件都加到song.txt中 

int delete_song();

int display_song();

int exist_song(char *buf);      //判断一首歌是否重复添加

#endif
