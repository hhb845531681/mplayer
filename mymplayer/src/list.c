#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>

#include "list.h"

#define SONG_FILE "song.txt"

int id = 0;

int write_song(Song *p)
{
	FILE *fp;
	fp =fopen("song.txt","a");
	if(fp == NULL)
	{
		perror("open fail\n");
		return 1;
	}
	fprintf(fp, "%-4d",p->songid);
	fprintf(fp, "%-100s\n",p->path);
	fclose(fp);
	return 0;
}

Song *read_song()  //从存歌的文件中读取歌的信息，形成链表，返回链头
{
	Song *head;
	Song *p,*pold,*pnew;
	FILE *fp;
	fp = fopen(SONG_FILE,"r");
	if(fp == NULL)
	{
		return NULL;
	}
	head = (Song *)malloc(sizeof(Song));
	memset(head,0,sizeof(Song));
	head->next = NULL;
	p = head;
	//pold = head;
	while(!feof(fp))
	{
		fscanf(fp,"%d",&p->songid);
		fscanf(fp,"%s",p->path);
		pnew = (Song *)malloc(sizeof(Song));
		memset(pnew,0,sizeof(Song));
		pnew->next = NULL;
		pold = p;
		p->next = pnew;
		//pold = p;
		p = pnew;
	}
	//pold->next = NULL;
	memset(p,0,sizeof(Song));
	free(p);
	pold->next = NULL;
	fclose(fp);
	return head;
}

int exist_song(char *buf)   //判断一首歌是否重复添加
{
	Song *p;
	p = read_song();
	while(p!=NULL)
	{
		if(strcmp(p->path,buf) == 0)
		{
			return 1;
		}
		p = p->next;
	}
	return 0;
}

int add_song()   //添加歌曲
{
	Song *p;
	char buf[100];

	p = (Song *)malloc(sizeof(Song));
	if(p == NULL)
	{
		perror("malloc fail\n");
		return 1;
	}
	memset(p,0,sizeof(Song));
	//getchar();
	//printf("please input songid\n");
	//scanf("%d",&songid);
	Song *head = read_song();
	if(head == NULL)
	{
		id = 0;
	}
	else
	{
		while(head->next != NULL)
		{
			id = head->songid;
			//printf("id:%d\n",id);
			//printf("songid:%d\n",head->songid);
			head = head->next;
		}
		//printf("%d\n",id);
	}
	//getchar();

	id ++;
	p->songid = id;
	getchar();
	printf("please input song's absolute path\n");
	scanf("%s",buf);
	//fgets(buf,sizeof(buf),stdin);

	if(access(buf,F_OK) != 0)   //判断文件是否存在，存在返回0
	{
		printf("this song is not exist,please go back input again\n");
		getchar();
		return 0;
	}
	else if(exist_song(buf) == 1)  //判断一首歌是否重复添加
	{
		printf("this song is existed,please do not input again\n");
		getchar();
		return 0;
	}
	else if(strcmp( buf + strlen(buf) - 3,"mp3") != 0)  //判断是否为.mp3
	{
		printf("it's not a mp3 file\n");
		getchar();
		return 0;
	}

	strcpy(p->path,buf);
	p->next = NULL;

	write_song(p);
	free(p);

	getchar();
	printf("add song success\n");
	printf("press any key to continue.\n");
	getchar();
	return 0;
}

int display_song()
{
	Song *p;
	p = read_song();
	if(p == NULL)
	{
		getchar();
		printf("文件中还没有歌曲\n");
		printf("press any key to continue\n");
		getchar();
		return 0;
	}
	printf("**********************\n");
	while(p->next != NULL)
	{
		printf("%04d,%s\n",p->songid,p->path);
		p = p->next;
	}
	getchar();
	printf("**********************\n");
	printf("display song success\n");
	printf("press any key to continue\n");
	getchar();
	return 0;
}

int delete_song()
{
	int t;  //确定删除哪首歌，输入其id
	int flag = 0;
	FILE *fp;
	Song *head;
	Song *p,*pold;
	head = read_song();
	p = head;
	pold = head;

	printf("which song do you want to delete,please input its songid:\n");
	scanf("%d",&t);

	if(head == NULL)
	{
		printf("system is empty now,please go back add song\n");
		return 0;
	}
	if(head->songid == t)
	{
		head = head->next;
		free(p);
		flag = 1;
	}
	else
	{
		while(p != NULL)
		{
			if(p->songid == t)
			{
				pold->next = p->next;
				free(p);
				flag = 1;
				break; 
			}
			else
			{
				pold = p;
				p = p->next;
			}
		}
	}
	if(flag == 0)
	{
		printf("sorry,this songid not exist\n");
		printf("press any key to continue.\n");
		getchar();
		return 0;
	}

	fp = fopen(SONG_FILE,"w");
	if(fp ==NULL)
	{
		perror("open file fail\n");
		return 1;
	}

	id = 0;
	while(head->next != NULL)  //重新写入一定要加->next,不然会多出一行0
	{
		id ++;
		head->songid = id;
		fprintf(fp, "%-4d",head->songid);
		fprintf(fp, "%-100s\n",head->path);
		head = head->next;
	}
	fclose(fp);

	getchar();
	printf("Delete song success\n");
	printf("press any key to continue.\n");
	getchar();
	return 0;
}


int add_folder()  //添加一个文件夹，将此文件夹下的.mp3文件都加到song.txt中
{
	char path[100];
	char tmp[60];  //
	Song *p;
	struct dirent *ptr;  //readdir  hanshu fanhui

	Song *head = read_song();
	if(head == NULL)
	{
		id = 0;
	}
	else
	{
		while(head != NULL)
		{
			id = head->songid;
			head = head->next;
		}
	}


	memset(path,0,sizeof(path));
	printf("please input the folder path:\n");
	scanf("%s",path);

	DIR *dir = opendir(path); //打开一个目录路径，返回一个目录指针

	if(dir == NULL)
	{
		printf("open dir:%s error\n",path);
		return 1;
	}
	while((ptr = readdir(dir)) != NULL)  //readdir读取目录指针中的信息，每次读一个，返回NULL表示读到目录尾了
	{     //返回一个dirent类型的结构体
		if(strcmp(ptr->d_name,".") == 0 || strcmp(ptr->d_name,"..") == 0)
			continue;

		if(ptr->d_type == 8)  //type为8表示这是一个文件
		{
			if(strcmp(ptr->d_name + strlen(ptr->d_name) - 3,"mp3") == 0)
			{
				id ++;
				memset(tmp,0,sizeof(tmp));
				strcpy(tmp,path);
				strcat(tmp,"/");
				strcat(tmp,ptr->d_name);
				p = (Song *)malloc(sizeof(Song));
				p->songid = id;
				strcpy(p->path,tmp);
				p->next = NULL;
				write_song(p);	    //判断为mp3文件后补齐路径，写入歌曲文件
				free(p);
			}
		}
	}
	closedir(dir);   

	getchar();
	printf("add song folder success\n");
	printf("press any key to continue.\n");
	getchar();
	return 0;
}