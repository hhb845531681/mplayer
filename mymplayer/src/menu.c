#include <stdio.h>
#include <stdlib.h>


#include "menu.h"
#include "list.h"

int songmenu()
{
	int x;
	printf("\t\t\t       Mplayer\n");
	printf("\t\t\t**********************\n");
	printf("\t\t\t     1.添加歌曲\n");
	printf("\t\t\t     2.添加文件夹下歌曲\n");
	printf("\t\t\t     3.删除歌曲\n");
	printf("\t\t\t     4.歌曲陈列\n");
	printf("\t\t\t     0.返回上一级\n");
	printf("\t\t\t**********************\n");
	printf("please select:\n");
	scanf("%d",&x);
	while(x<0 || x>5)
	{
		printf("input error,again:\n");
		scanf("%d",&x);
	}
	return x;
}


void songsheet()  //ge dan
{
	int x,loop = 1;
	while(loop)
	{
		system("clear");
		printf("歌单操作\n");
		x = songmenu();
		switch(x)
		{
			case 1:
				add_song();
				break;
			case 2:
				add_folder();
				break;
			case 3:
				delete_song();
				break;
			case 4:
				display_song();
				break;
			case 0:
			default:
				loop = 0;
				break;
		}
	}
}